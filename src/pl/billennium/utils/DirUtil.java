package pl.billennium.utils;

import org.apache.commons.io.FileUtils;
import pl.billennium.ExecutionParameter;
import pl.billennium.config.Configuration;
import pl.billennium.config.ConfigurationService;
import pl.billennium.steps.register.DownloadedFilesCollector;

import java.io.File;
import java.io.IOException;

public class DirUtil {

    public static void clearWorkingDir() {
        Configuration configuration = ConfigurationService.getInstance().getConfiguration();
        try {
            FileUtils.cleanDirectory(new File(configuration.getWorkingDir()));
            DownloadedFilesCollector.clearDirsAndFilesRegistry();
            System.out.println("Folder pusty");
        } catch (IOException e) {
            System.out.println("Nie udało się wyczyścić folderu");
        }
    }

    public static void deleteSessionDir(ExecutionParameter parameter) {
        try {
            FileUtils.deleteDirectory(new File(parameter.getSessionDir().toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}