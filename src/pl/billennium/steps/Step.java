package pl.billennium.steps;

import pl.billennium.ExecutionParameter;
import pl.billennium.steps.search.SearchResult;

import java.util.List;

/**
 * Created by ebos on 10/31/2017.
 */
public interface Step {

    String getName();

    boolean isSuccess();

    void execute(ExecutionParameter parameter);

    default boolean isProductive() {
        return false;
    }

    default List<SearchResult> getResults() {
        return null;
    }

}
