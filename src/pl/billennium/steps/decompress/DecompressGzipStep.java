package pl.billennium.steps.decompress;

import org.apache.commons.io.FileUtils;
import pl.billennium.ExecutionParameter;
import pl.billennium.steps.Step;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ebos on 10/31/2017.
 */
public class DecompressGzipStep implements Step {

    @Override
    public String getName() {
        return "DecompressGzipStep";
    }

    @Override
    public boolean isSuccess() {
        return true;
    }


    @Override
    public void execute(ExecutionParameter parameter) {
        Collection<File> files = FileUtils.listFiles(parameter.getSessionDir(), new String[]{"gz"}, false);

        ExecutorService es = Executors.newFixedThreadPool(10);

        List<DecompressGzipSingleFileTask> tasks = new ArrayList<>();
        files.stream().forEach(a -> tasks.add(new DecompressGzipSingleFileTask(a)));
        try {
            es.invokeAll(tasks);
        } catch (Exception e) {
            System.out.println("błąd! decompress");
            throw new RuntimeException();
        }
        es.shutdown();
    }
}