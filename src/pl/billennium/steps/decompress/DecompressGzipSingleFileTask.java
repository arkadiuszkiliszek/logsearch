package pl.billennium.steps.decompress;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;

/**
 * Created by ebos on 11/2/2017.
 */
public class DecompressGzipSingleFileTask implements Callable<String> {

    private File gzipFile;

    public DecompressGzipSingleFileTask(File gzipFile) {
        this.gzipFile = gzipFile;
    }

    @Override
    public String call() throws Exception {
        try {
            GZIPInputStream d = new GZIPInputStream(new FileInputStream(gzipFile));
            IOUtils.copy(d, new FileOutputStream(outputFile(gzipFile.getParentFile(), gzipFile)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    private File outputFile(File dir, File file) {
        return new File(dir, file.getName().replace(".gz", ""));
    }

}