package pl.billennium.steps.download;

import com.jcraft.jsch.SftpException;
import org.apache.commons.io.FileUtils;
import pl.billennium.config.Configuration;
import pl.billennium.config.ConfigurationService;
import pl.billennium.config.Server;

import java.io.File;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Stream;

public class DownloadFromNetworkDiskTask extends DownloadLogsTask {

    private static final Date currentDate = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());

    public DownloadFromNetworkDiskTask(DownloadLogsTaskArg arg) {
        super(arg);
    }

    @Override
    public String call() throws Exception {

        // TODO sprawdzic formatowanie stringow pod wzgledem optymalizacji

        Configuration config = ConfigurationService.getInstance().getConfiguration();
        String dirPath = String.format("%s:\\%s\\", arg.getNetworkDisk().getLocalDiskName(), config.getNetworkDirName());
        Server server = arg.getServer();

        Map<String, Path> namesAndPaths = new HashMap<>();

        for (String node : arg.getNodes()) {
            String filePattern = prepareNamePattern(arg);
            String dirWithLogs = String.format("%sapp0%d_%s", dirPath, server.getNumber(), node);
            String dirWithLogsToday = String.format("%s\\today\\", dirWithLogs);
            String dirWithLogsArchive = String.format("%s\\archive\\", dirWithLogs);

            if (!arg.getDateFrom().before(currentDate)) {

                filterDirWithLogs(filePattern, dirWithLogsToday, namesAndPaths);

            } else if (arg.getDateTo().before(currentDate)) {

                filterDirWithLogs(filePattern, dirWithLogsArchive, namesAndPaths);

            } else {
                filterDirWithLogs(filePattern, dirWithLogsToday, namesAndPaths);
                filterDirWithLogs(filePattern, dirWithLogsArchive, namesAndPaths);
            }

        }

        downloadForCurrentDir(namesAndPaths, config);
        return null;
    }

    private void filterDirWithLogs(String filePattern, String dirWithLogs, Map<String, Path> namesAndPaths) {
        try (Stream<Path> paths = Files.walk(Paths.get(dirWithLogs))) {
            paths.filter(p -> p.getFileName().toString().equals(filePattern))
                    .forEach(path -> namesAndPaths.put(path.getFileName().toString(), path));
        } catch (IOException e) {
            System.out.println("blad przy pobieraniu");
        }
    }

    private void downloadForCurrentDir(Map<String, Path> namesAndPaths, Configuration config) throws SftpException, IOException {
        DateFilterStrategy dateFilterStrategy = filterStrategies.get(arg.getDateFilterType());

        Set<String> fileNames = namesAndPaths.keySet();

        Set<String> filteredFileNames = dateFilterStrategy.filter(fileNames, arg);

        for (String filteredName : filteredFileNames) {
            Path filteredNamePath = namesAndPaths.get(filteredName);

            String node = filteredNamePath.toString().substring(16, 17);

            File fileToCopy = new File(String.valueOf(filteredNamePath));
            File destDir = prepareFilePath(arg, node, config.getWorkingDir(), fileToCopy.getName());
//            FileUtils.copyFileToDirectory(fileToCopy, destDir);
            FileUtils.copyFile(fileToCopy, destDir);
            System.out.println(fileToCopy.getName());
        }
    }

    private static String prepareNamePattern(DownloadLogsTaskArg arg) {

        String date = String.format("%s-%s", arg.getDateFrom().toLocaleString().substring(0, 10), arg.getDateFrom().toLocaleString().substring(11, 13));
        String filePattern = arg.getFilePattern();

        return String.format("%s%s.gz", filePattern.replace("*", "log."), date);
    }
}
