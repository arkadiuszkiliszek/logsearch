package pl.billennium.steps.download;

import com.google.common.base.Strings;
import com.jcraft.jsch.*;
import pl.billennium.config.Configuration;
import pl.billennium.config.ConfigurationService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Callable;

/**
 * Created by ebos on 10/31/2017.
 */
public class DownloadLogsTask implements Callable<String> {

    protected Map<DownloadLogsTaskArg.DATE_FILTER_TYPE, DateFilterStrategy> filterStrategies;

    protected DownloadLogsTaskArg arg;

    public DownloadLogsTask(DownloadLogsTaskArg arg) {
        this.arg = arg;
        this.filterStrategies = new HashMap<>();
        this.filterStrategies.put(DownloadLogsTaskArg.DATE_FILTER_TYPE.NONE, new NoneDateFilterStrategy());
        this.filterStrategies.put(DownloadLogsTaskArg.DATE_FILTER_TYPE.HOUR, new SingleHourDateFilterStrategy());
        this.filterStrategies.put(DownloadLogsTaskArg.DATE_FILTER_TYPE.PERIOD, new PeriodDateFilterStrategy());
    }

    protected File prepareFilePath(DownloadLogsTaskArg arg, String node, String workingDir, String fileName) {
        return new File(workingDir, arg.getSearchSession() + File.separator + String.format("%s_%s_%s",
                arg.getServer().getNumber(), node, fileName));
    }

    @Override
    public String call() throws Exception {
        Configuration config = ConfigurationService.getInstance().getConfiguration();
        JSch jsch = new JSch();
        try {
            if (config.getPassphrase() != null) {
                jsch.addIdentity(config.getKeyPath(), config.getPassphrase());
            } else {
                jsch.addIdentity(config.getKeyPath());
            }

            Session session = jsch.getSession(config.getUser(), arg.getServer().getHost(), arg.getServer().getPort());

            java.util.Properties jschConfig = new java.util.Properties();
            jschConfig.put("StrictHostKeyChecking", "no");
            session.setConfig(jschConfig);
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            System.out.println("shell channel connected....");

            ChannelSftp c = (ChannelSftp) channel;
            for (String node : arg.getNodes()) {
                c.cd(node);
                downloadForCurrentDir(c, node, config);
                downloadFromArchiveIfNeeded(c, node, config);
                c.cd("..");
            }

            c.disconnect();
            session.disconnect();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    private void downloadFromArchiveIfNeeded(ChannelSftp c, String node, Configuration config) throws SftpException, FileNotFoundException {
        if (!Strings.isNullOrEmpty(config.getArchiveDir())) {
            if (remoteFileExists(c, config.getArchiveDir())) {
                c.cd(config.getArchiveDir());
                downloadForCurrentDir(c, node, config);
                c.cd("..");
            }
        }
    }

    private boolean remoteFileExists(ChannelSftp c, String name) throws SftpException {
        try {
            c.lstat(name);
            return true;
        } catch (SftpException e) {
            if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
                return false;
            } else {
                // something else went wrong
                throw e;
            }
        }
    }

    private void downloadForCurrentDir(ChannelSftp c, String node, Configuration config) throws SftpException, FileNotFoundException {
        Vector<ChannelSftp.LsEntry> list = c.ls(arg.getFilePattern());
        DateFilterStrategy dateFilterStrategy = filterStrategies.get(arg.getDateFilterType());
        list = dateFilterStrategy.filter(list, arg);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getFilename());
            c.get(list.get(i).getFilename(), new FileOutputStream(prepareFilePath(arg, node, config.getWorkingDir(),
                    list.get(i).getFilename())));
        }
    }

}
