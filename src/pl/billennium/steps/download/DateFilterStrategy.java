package pl.billennium.steps.download;

import com.jcraft.jsch.ChannelSftp;
import pl.billennium.config.ConfigurationService;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ebos on 11/2/2017.
 */
interface DateFilterStrategy {

    Vector<ChannelSftp.LsEntry> filter(Vector<ChannelSftp.LsEntry> list, DownloadLogsTaskArg arg);

    Set<String> filter(Set<String> fileNames, DownloadLogsTaskArg arg);
}

class SingleHourDateFilterStrategy implements DateFilterStrategy {

    @Override
    public Vector<ChannelSftp.LsEntry> filter(Vector<ChannelSftp.LsEntry> list, DownloadLogsTaskArg arg) {
        Vector<ChannelSftp.LsEntry> result = new Vector<>();

        SimpleDateFormat sdf = ConfigurationService.getInstance().getConfiguration().prepareDateFormat();
        String format = sdf.format(arg.getDateFrom());

        for (ChannelSftp.LsEntry entry : list) {
            if (entry.getFilename().contains(format)) {
                result.add(entry);
            }
        }

        return result;
    }

    @Override
    public Set<String> filter(Set<String> fileNames, DownloadLogsTaskArg arg) {
        Set<String> result = new HashSet<>();

        SimpleDateFormat sdf = ConfigurationService.getInstance().getConfiguration().prepareDateFormat();
        String format = sdf.format(arg.getDateFrom());

        for (String name : fileNames) {
            if (name.contains(format)) {
                result.add(name);
            }
        }

        return result;
    }
}

class PeriodDateFilterStrategy implements DateFilterStrategy {

    @Override
    public Vector<ChannelSftp.LsEntry> filter(Vector<ChannelSftp.LsEntry> list, DownloadLogsTaskArg arg) {
        Vector<ChannelSftp.LsEntry> result = new Vector<>();
        Date prepareDateFrom = prepareDateFrom(arg.getDateFrom());
        Date prepareDateTo = prepareDateTo(arg.getDateTo());

        for (ChannelSftp.LsEntry entry : list) {
            Date date = fetchDate(entry.getFilename());
            if (date == null) {
                Date now = prepareNowHour();
                if (now.after(prepareDateFrom) && now.before(prepareDateTo)) {
                    result.add(entry);
                }
                continue;
            }

            if (date.after(prepareDateFrom) && date.before(prepareDateTo)) {
                result.add(entry);
            }
        }

        return result;
    }

    @Override
    public Set<String> filter(Set<String> fileNames, DownloadLogsTaskArg arg) {
        Set<String> result = new HashSet<>();

        Date prepareDateFrom = prepareDateFrom(arg.getDateFrom());
        Date prepareDateTo = prepareDateTo(arg.getDateTo());

        for (String name : fileNames) {
            Date date = fetchDate(name);
            if (date == null) {
                Date now = prepareNowHour();
                if (now.after(prepareDateFrom) && now.before(prepareDateTo)) {
                    result.add(name);
                }
                continue;
            }

            if (date.after(prepareDateFrom) && date.before(prepareDateTo)) {
                result.add(name);
            }
        }

        return result;
    }

    private Date prepareNowHour() {
        Calendar tmp = Calendar.getInstance();
        tmp.set(Calendar.MINUTE, 0);
        tmp.set(Calendar.SECOND, 0);
        tmp.set(Calendar.MILLISECOND, 0);

        return tmp.getTime();
    }

    private Date fetchDate(String fileName) {
        if (fileName.toLowerCase().endsWith(".log")) {
            return null;
        }

        SimpleDateFormat sdf = ConfigurationService.getInstance().getConfiguration().prepareDateFormat();
        String strDate = fileName.replace(".gz", "");
        strDate = strDate.substring(strDate.lastIndexOf(".") + 1);

        try {
            return sdf.parse(strDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private Date prepareDateFrom(Date date) {
        Calendar tmp = Calendar.getInstance();
        tmp.setTime(date);
        tmp.set(Calendar.MINUTE, 0);
        tmp.set(Calendar.SECOND, 0);
        tmp.set(Calendar.MILLISECOND, 0);
        tmp.add(Calendar.MINUTE, -1);

        return tmp.getTime();
    }

    private Date prepareDateTo(Date date) {
        Calendar tmp = Calendar.getInstance();
        tmp.setTime(date);
        tmp.set(Calendar.MINUTE, 0);
        tmp.set(Calendar.SECOND, 0);
        tmp.set(Calendar.MILLISECOND, 0);
        tmp.add(Calendar.MINUTE, 1);

        return tmp.getTime();
    }

}

class NoneDateFilterStrategy implements DateFilterStrategy {

    @Override
    public Vector<ChannelSftp.LsEntry> filter(Vector<ChannelSftp.LsEntry> list, DownloadLogsTaskArg arg) {
        return new Vector<>(list);
    }

    @Override
    public Set<String> filter(Set<String> fileNames, DownloadLogsTaskArg arg) {
        return fileNames;
    }
}


