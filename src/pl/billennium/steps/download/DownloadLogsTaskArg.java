package pl.billennium.steps.download;

import pl.billennium.config.NetworkDisk;
import pl.billennium.config.Server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Created by ebos on 10/31/2017.
 */
public class DownloadLogsTaskArg {

    public enum DATE_FILTER_TYPE {
        NONE,
        HOUR,
        PERIOD
    }

    private String searchSession;
    private String filePattern;
    private Server server;
    private NetworkDisk networkDisk;
    private List<String> nodes;
    private Date dateFrom;
    private Date dateTo;

    private DownloadLogsTaskArg(Builder builder) {
        this.searchSession = builder.searchSession;
        this.filePattern = builder.filePattern;
        this.server = builder.server;
        this.nodes = new ArrayList<>(builder.nodes);
        this.dateFrom = builder.dateFrom;
        this.dateTo = builder.dateTo;
        this.networkDisk = builder.networkDisk;
    }

    public String getSearchSession() {
        return searchSession;
    }

    public String getFilePattern() {
        return filePattern;
    }

    public Server getServer() {
        return server;
    }

    public List<String> getNodes() {
        return nodes;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public NetworkDisk getNetworkDisk() {
        return networkDisk;
    }

    public boolean isSingleHourRestricted() {
        return dateFrom != null && dateTo == null;
    }

    public boolean isPeriodRestricted() {
        return dateFrom != null && dateTo != null;
    }

    public DATE_FILTER_TYPE getDateFilterType() {
        if (isSingleHourRestricted()) {
            return DATE_FILTER_TYPE.HOUR;
        } else if (isPeriodRestricted()) {
            return DATE_FILTER_TYPE.PERIOD;
        } else {
            return DATE_FILTER_TYPE.NONE;
        }
    }

    public static class Builder {
        private String searchSession;
        private String filePattern;
        private Server server;
        private List<String> nodes;
        private Date dateFrom;
        private Date dateTo;
        private NetworkDisk networkDisk;

        public Builder() {
            this.nodes = new ArrayList<>();
        }

        public Builder searchSession(String searchSession) {
            this.searchSession = searchSession;
            return this;
        }

        public Builder filePattern(String filePattern) {
            this.filePattern = filePattern;
            return this;
        }

        public Builder server(Server server) {
            this.server = server;
            return this;
        }

        public Builder nodes(String... node) {
            this.nodes.addAll(Arrays.asList(node));
            return this;
        }

        public Builder dateFrom(Date dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public Builder dateTo(Date dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public Builder networkDisk(NetworkDisk networkDisk){
            this.networkDisk = networkDisk;
            return this;
        }

        public DownloadLogsTaskArg build() {
            checkNotNull(searchSession, "searchSession == null");
            checkNotNull(filePattern, "filePattern == null");
            checkState(server  != null || networkDisk !=null, "server == null or networkDisk == null");
            checkState(!nodes.isEmpty(), "nodes.isEmpty()");
            return new DownloadLogsTaskArg(this);
        }

    }

}
