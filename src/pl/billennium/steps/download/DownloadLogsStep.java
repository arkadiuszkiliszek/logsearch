package pl.billennium.steps.download;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import pl.billennium.ExecutionParameter;
import pl.billennium.config.Configuration;
import pl.billennium.config.ConfigurationService;
import pl.billennium.config.Server;
import pl.billennium.steps.Step;
import pl.billennium.utils.DirUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Created by ebos on 10/31/2017.
 */
public class DownloadLogsStep implements Step {
    @Override
    public String getName() {
        return "DownloadLogsStep";
    }

    @Override
    public boolean isSuccess() {
        return true;
    }

    @Override
    public void execute(ExecutionParameter parameter) {
        ExecutorService es = Executors.newFixedThreadPool(10);

        List<DownloadLogsTask> downloadLogsTasks = prepareTasks(parameter);
        try {
            es.invokeAll(downloadLogsTasks);
        } catch (Exception e) {
            System.out.println("Błąd! " + e.getMessage());
            DirUtil.deleteSessionDir(parameter);
            throw new RuntimeException(e);
        }
        System.out.println("wszystkie sciagniete");
        es.shutdown();
    }

    private List<DownloadLogsTask> prepareTasks(ExecutionParameter parameter) {
        String[] servers = parameter.getServers().split(",");
        Multimap<Integer, Integer> mapping = MultimapBuilder.hashKeys().hashSetValues().build();
        for (String server : servers) {
            String[] split = server.split("-");
            mapping.put(Integer.valueOf(split[0]), Integer.valueOf(split[1]));
        }

        Configuration configuration = ConfigurationService.getInstance().getConfiguration();
        List<DownloadLogsTask> result = new ArrayList<>();
        if (!parameter.getOnNetworkDrive()) {
            getTaskToDownloadFromRemoteServers(parameter, mapping, configuration, result);
        } else {
            getTaskToDownloadFromNetworkDisk(parameter, mapping, configuration, result);
        }
        return result;
    }

    /**
     * Metoda generuje zadania pobierania logow ze zmapowanych dyskow sieciowych - prd$, uat$ etc
     *
     * @param parameter
     * @param mapping
     * @param configuration
     * @param result
     */
    private void getTaskToDownloadFromNetworkDisk(ExecutionParameter parameter, Multimap<Integer, Integer> mapping, Configuration configuration, List<DownloadLogsTask> result) {
        for (Integer serverKey : mapping.keySet()) {
            Server server = configuration.getServers().stream().filter(a -> a.getNumber().equals(serverKey)).findAny().get();
            List<String> instances = mapping.get(serverKey)
                    .stream()
                    .map(instanceValue -> server.getNodes()
                            .stream()
                            .filter(a -> a.getNumber().equals(instanceValue)).findAny().get().getName())
                    .collect(Collectors.toList());

            DownloadLogsTaskArg args = new DownloadLogsTaskArg.Builder().
                    networkDisk(parameter.getNetworkDisk()).filePattern(parameter.getFilePattern()).
                    server(server)
                    .dateFrom(parameter.getDateFrom())
                    .dateTo(parameter.getDateTo())
                    .nodes(instances.toArray(new String[0]))
                    .searchSession(parameter.getSearchSession()).build();

            result.add(new DownloadFromNetworkDiskTask(args));
        }
    }

    /**
     * Metoda generuje zadania pobierania logow z serwerow zdalnych - logi przechowywane po staremu
     *
     * @param parameter
     * @param mapping
     * @param configuration
     * @param result
     */
    private void getTaskToDownloadFromRemoteServers(ExecutionParameter parameter, Multimap<Integer, Integer> mapping, Configuration configuration, List<DownloadLogsTask> result) {
        for (Integer serverKey : mapping.keySet()) {
            Server server = configuration.getServers().stream().filter(a -> a.getNumber().equals(serverKey)).findAny().get();
            List<String> instances = mapping.get(serverKey)
                    .stream()
                    .map(instanceValue -> server.getNodes()
                            .stream()
                            .filter(a -> a.getNumber().equals(instanceValue)).findAny().get().getName())
                    .collect(Collectors.toList());

            DownloadLogsTaskArg arg = new DownloadLogsTaskArg.Builder().server(server).filePattern(parameter.getFilePattern())
                    .dateFrom(parameter.getDateFrom()).dateTo(parameter.getDateTo())
                    .searchSession(parameter.getSearchSession()).nodes(instances.toArray(new String[0])).build();
            result.add(new DownloadLogsTask(arg));
        }
    }

}
