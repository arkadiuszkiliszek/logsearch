package pl.billennium.steps;

import pl.billennium.ExecutionParameter;

import java.io.File;

/**
 * Created by ebos on 10/31/2017.
 */
public class PrepareWorkingSpaceStep implements Step {
    private boolean succcess = false;

    @Override
    public String getName() {
        return "prepare";
    }

    @Override
    public boolean isSuccess() {
        return succcess;
    }

    @Override
    public void execute(ExecutionParameter parameter) {
        File sessionDir = parameter.getSessionDir();
        succcess = sessionDir.mkdir();
    }

}
