package pl.billennium.steps.inspect;

import pl.billennium.ExecutionParameter;
import pl.billennium.config.Configuration;
import pl.billennium.config.ConfigurationService;
import pl.billennium.steps.Step;
import pl.billennium.steps.register.DownloadedFilesCollector;
import pl.billennium.steps.search.SearchResult;
import pl.billennium.steps.search.SearchTaskSinglePhrase;
import pl.billennium.steps.search.SearchTaskTwoPhrases;

import java.io.File;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class InspectWorkingDirStep implements Step {

    private DownloadedFilesCollector downloadedFilesCollector = DownloadedFilesCollector.getInstance();

    private List<String> dirsWithFoundFiles = new ArrayList<>();

    private boolean found = false;

    private List<SearchResult> results;

    @Override
    public String getName() {
        return "InspectWorkingDirStep";
    }

    @Override
    public boolean isSuccess() {
        return true;
    }

    @Override
    public void execute(ExecutionParameter parameter) {
        if (findInDir(parameter)) {
            System.out.println("Nie trzeba pobierac");
            search(parameter);
        } else {
            this.found = false;
        }
    }

    private boolean findInDir(ExecutionParameter parameter) {

        List<String> namePatterns = prepareNamePatterns(parameter);

        Map<String, List<String>> dirsAndFiles = downloadedFilesCollector.getDirsAndFiles();
        List<String> allFiles = dirsAndFiles.values().stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());

        if (allFiles.containsAll(namePatterns)) {
            for (Map.Entry<String, List<String>> entry : dirsAndFiles.entrySet()) {
                if (entry.getValue().stream().anyMatch(namePatterns::contains)) {
                    this.getDirsWithFoundFiles().add(entry.getKey());
                    this.found = true;
                }
            }
        }
        return found;
    }

    private void search(ExecutionParameter parameter) {
        System.out.println("Szukam w pobranych");
        ExecutorService es = Executors.newFixedThreadPool(10);
        List<SearchResult> results = new ArrayList<>();
        List<File> fileList = preapareFilesForSearching(parameter);

        List<Callable<List<SearchResult>>> tasks = new ArrayList<>();

        if (parameter.isSinglePhraseSearch()) {
            fileList.forEach(a -> tasks.add(new SearchTaskSinglePhrase(a, parameter.getPhrase1())));
        } else {
            fileList.forEach(a -> tasks.add(new SearchTaskTwoPhrases(a, parameter.getPhrase1(), parameter.getPhrase2())));
        }

        try {
            List<Future<List<SearchResult>>> futures = es.invokeAll(tasks);
            futures.forEach(a -> {
                try {
                    results.addAll(a.get());
                } catch (InterruptedException | ExecutionException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        es.shutdown();

        this.results = results;
    }

    public List<String> getDirsWithFoundFiles() {
        return dirsWithFoundFiles;
    }

    @Override
    public boolean isProductive() {
        return found;
    }

    @Override
    public List<SearchResult> getResults() {
        return results;
    }

    private List<File> preapareFilesForSearching(ExecutionParameter parameter) {
        Configuration config = ConfigurationService.getInstance().getConfiguration();
        List<String> namePatterns = prepareNamePatterns(parameter);
        List<File> filesForSearching = new ArrayList<>();

        for (String dir : dirsWithFoundFiles) {
            File directory = new File(config.getWorkingDir() + "\\" + dir);
            File[] filesInDirectory = directory.listFiles((dir1, name) -> !name.endsWith("gz"));
            List<File> filesMatchingName = Arrays.stream(filesInDirectory).filter(file -> namePatterns.contains(file.getName())).collect(Collectors.toList());
            filesForSearching.addAll(filesMatchingName);
        }

        Collections.sort(filesForSearching);

        removeDuplicateFilesForSearching(filesForSearching);

        return filesForSearching;
    }

    private List<String> prepareNamePatterns(ExecutionParameter parameter) {
        String[] servers = parameter.getServers().split(",");
        String date = parameter.getDateFrom().toLocaleString().substring(0, 10) + "-" + parameter.getDateFrom().toLocaleString().substring(11, 13);
        String filePattern = parameter.getFilePattern();
        List<String> namePatterns = new ArrayList<>();
        for (String server : servers) {
            String[] split = server.split("-");
            namePatterns.add(split[0] + "_n" + split[1] + "_" + filePattern.substring(0, filePattern.length() - 1) + "log." + date);
        }
        return namePatterns;
    }

    private void removeDuplicateFilesForSearching(List<File> filesForSearching) {
        for (int i = 1; i < filesForSearching.size(); i++) {
            File previousFile = filesForSearching.get(i - 1);
            File currentFile = filesForSearching.get(i);
            if (previousFile.getName().equals(currentFile.getName())) {
                long previousFileTimestamp = previousFile.lastModified();
                long currentFileTimestamp = currentFile.lastModified();
                if (previousFileTimestamp > currentFileTimestamp) {
                    filesForSearching.remove(currentFile);
                } else {
                    filesForSearching.remove(previousFile);
                }
            }
        }
    }
}