package pl.billennium.steps;

import pl.billennium.ExecutionParameter;
import pl.billennium.steps.decompress.DecompressGzipStep;
import pl.billennium.steps.register.DownloadedFilesCollector;
import pl.billennium.steps.download.DownloadLogsStep;
import pl.billennium.steps.inspect.InspectWorkingDirStep;
import pl.billennium.steps.search.SearchResult;
import pl.billennium.steps.search.SearchStep;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebos on 10/31/2017.
 */
public class StepsManager {

    private List<Step> steps;

    private List<SearchResult> results;

    public StepsManager() {
        this.steps = new ArrayList<>();
        this.steps.add(new InspectWorkingDirStep());
        this.steps.add(new PrepareWorkingSpaceStep());
        this.steps.add(new DownloadLogsStep());
        this.steps.add(new DecompressGzipStep());
        this.steps.add(DownloadedFilesCollector.getInstance());
        this.steps.add(new SearchStep());
    }

    public List<SearchResult> execute(ExecutionParameter parameter) {
        for (int i = 0; i < steps.size(); i++) {
            Step step = steps.get(i);
            System.out.println("Krok " + step.getName());

            step.execute(parameter);
            if (!step.isSuccess()) {
                System.out.println("Blad przerywam");
                break;
            } else {
                System.out.println("Pomyslnie wykonano");
                if (step.isProductive()) {
                    results = step.getResults();
                    System.out.println(String.format("Znaleziono %s trafien", results.size()));
                    break;
                }
            }
        }
        return results;
    }
}
