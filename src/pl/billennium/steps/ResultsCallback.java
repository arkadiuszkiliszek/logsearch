package pl.billennium.steps;

import pl.billennium.steps.search.SearchResult;

import java.util.List;

/**
 * Created by ebos on 11/10/2017.
 */
public interface ResultsCallback {

    void resultsCompleted(List<SearchResult> results);

}
