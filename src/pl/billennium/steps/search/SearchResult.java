package pl.billennium.steps.search;

import java.io.File;

/**
 * Created by ebos on 10/31/2017.
 */
public class SearchResult {
    private File file;
    private String line;
    private Integer lineNumber;
    private String logMessage;

    public SearchResult(File file, String line, Integer lineNumber) {
        this.file = file;
        this.line = line;
        this.lineNumber = lineNumber;
    }

    public File getFile() {
        return file;
    }

    public String getLine() {
        return line;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public boolean isLogMessagePopulated() {
        return logMessage != null;
    }

    public String getLogMessage() {
        return logMessage;
    }

    public void setLogMessage(String logMessage) {
        this.logMessage = logMessage;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "file=" + file +
                ", line='" + line + '\'' +
                ", lineNumber=" + lineNumber +
                ", logMessage='" + logMessage + '\'' +
                '}';
    }

}
