package pl.billennium.steps.search;

import pl.billennium.ExecutionParameter;
import pl.billennium.steps.Step;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by ebos on 10/31/2017.
 */
public class SearchStep implements Step {

    private List<SearchResult> results;

    @Override
    public String getName() {
        return "SearchStep";
    }

    @Override
    public boolean isSuccess() {
        return true;
    }

    @Override
    public void execute(ExecutionParameter parameter) {
        ExecutorService es = Executors.newFixedThreadPool(10);
        List<SearchResult> results = new ArrayList<>();

        File[] files = parameter.getSessionDir().listFiles((dir1, name) -> !name.endsWith("gz"));
        List<File> fileList = Arrays.asList(files);

        List<Callable<List<SearchResult>>> tasks = new ArrayList<>();

        if (parameter.isSinglePhraseSearch()) {
            fileList.forEach(a -> tasks.add(new SearchTaskSinglePhrase(a, parameter.getPhrase1())));
        } else {
            fileList.forEach(a -> tasks.add(new SearchTaskTwoPhrases(a, parameter.getPhrase1(), parameter.getPhrase2())));
        }

        try {
            List<Future<List<SearchResult>>> futures = es.invokeAll(tasks);
            futures.forEach(a -> {
                try {
                    results.addAll(a.get());
                } catch (InterruptedException | ExecutionException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        es.shutdown();

        this.results = results;
    }

    @Override
    public boolean isProductive() {
        return true;
    }

    @Override
    public List<SearchResult> getResults() {
        return results;
    }

}
