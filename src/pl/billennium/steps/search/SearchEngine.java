package pl.billennium.steps.search;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ebos on 10/31/2017.
 */
public class SearchEngine {
    private final static String REGEX = "^(\\d{4}-\\d{2}-\\d{2}) (\\d{2}:\\d{2}:\\d{2},\\d{3}) (INFO|DEBUG|WARN|ERROR).*$";

    public SearchEngine() {
    }

    public List<SearchResult> searchWithinFile(File file, String phrase1) {
        List<SearchResult> result = new ArrayList<>();
        LogMessageHandler logMessageHandler = new LogMessageHandler(searchResultList -> result.addAll(searchResultList));

        searchSingleFile(file, phrase1, logMessageHandler);
        return result;
    }

    public List<SearchResult> searchWithinFile(File file, String phrase1, String phrase2) {
        List<SearchResult> result = new ArrayList<>();
        LogMessageHandler logMessageHandler = new LogMessageHandler(searchResultList -> result.addAll(filterByPhrase2WithinLogMessage(searchResultList, phrase2)));

        searchSingleFile(file, phrase1, logMessageHandler);

        return result;
    }

    public List<SearchResult> searchWithinDir(File dir, String phrase1) {
        File[] files = dir.listFiles((dir1, name) -> !name.endsWith("gz"));
        List<SearchResult> result = new ArrayList<>();
        LogMessageHandler logMessageHandler = new LogMessageHandler(searchResultList -> result.addAll(searchResultList));

        for (File f : files) {
            searchSingleFile(f, phrase1, logMessageHandler);
        }

        return result;
    }

    public List<SearchResult> search(File dir, String phrase1, String phrase2) {
        File[] files = dir.listFiles((dir1, name) -> !name.endsWith("gz"));
        List<SearchResult> result = new ArrayList<>();
        LogMessageHandler logMessageHandler = new LogMessageHandler(searchResultList -> result.addAll(filterByPhrase2WithinLogMessage(searchResultList, phrase2)));

        for (File f : files) {
            searchSingleFile(f, phrase1, logMessageHandler);
        }

        return result;
    }

    private void searchSingleFile(File file, String phrase1, LogMessageHandler logMessageHandler) {
        String line;
        int lineNumber = 1;
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            while ((line = br.readLine()) != null) {
                logMessageHandler.checkLine(line);

                if (logMessageHandler.isNextMessage() & line.toLowerCase().contains(phrase1.toLowerCase())) {
                    SearchResult searchResult = new SearchResult(file, line, lineNumber);
                    logMessageHandler.registerResult(searchResult);
                }
                lineNumber++;
            }
            logMessageHandler.endOfFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<SearchResult> filterByPhrase2WithinLogMessage(List<SearchResult> searchResults, String phrase2) {
        List<SearchResult> result = searchResults.stream().filter(a -> a.getLogMessage().contains(phrase2))
                .collect(Collectors.toList());
        return result;
    }

}
