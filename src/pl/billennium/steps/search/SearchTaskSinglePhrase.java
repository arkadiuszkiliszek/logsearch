package pl.billennium.steps.search;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ebos on 11/2/2017.
 */
public class SearchTaskSinglePhrase implements Callable<List<SearchResult>> {

    private File file;
    private String phrase1;

    public SearchTaskSinglePhrase(File file, String phrase1) {
        checkNotNull(file);
        checkNotNull(phrase1);
        this.file = file;
        this.phrase1 = phrase1;
    }

    @Override
    public List<SearchResult> call() throws Exception {
        SearchEngine engine = new SearchEngine();
        return engine.searchWithinFile(file, phrase1);
    }

}
