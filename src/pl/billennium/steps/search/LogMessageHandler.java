package pl.billennium.steps.search;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ebos on 11/2/2017.
 */
public class LogMessageHandler {

    private final static String REGEX = "^(\\d{4}-\\d{2}-\\d{2}) (\\d{2}:\\d{2}:\\d{2},\\d{3}) (INFO|DEBUG|WARN|ERROR).*$";

    private List<String> logMessageLines;
    private List<SearchResult> registeredResults;
    private LogMessageFeedCallback callback;
    private boolean isNextMessage;

    public LogMessageHandler(LogMessageFeedCallback callback) {
        this.callback = callback;
        this.logMessageLines = new ArrayList<>();
        this.registeredResults = new ArrayList<>();
        this.isNextMessage = true;
    }

    public void checkLine(String line) {
        if (line.matches(REGEX)) {
            feedResults();
            isNextMessage = true;
        }
        logMessageLines.add(line);
    }

    public void registerResult(SearchResult result) {
        registeredResults.add(result);
        isNextMessage = false;
    }

    public void endOfFile() {
        feedResults();
    }

    private void feedResults() {
        String msg = logMessageLines.stream().collect(Collectors.joining("\n"));
        registeredResults.forEach(a -> a.setLogMessage(msg));
        callback.fed(ImmutableList.copyOf(registeredResults));
        registeredResults.clear();
        logMessageLines.clear();
    }

    public boolean isNextMessage() {
        return isNextMessage;
    }

    public void setNextMessage(boolean nextMessage) {
        isNextMessage = nextMessage;
    }
}
