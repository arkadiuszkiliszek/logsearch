package pl.billennium.steps.search;

import com.google.common.base.Preconditions;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ebos on 11/2/2017.
 */
public class SearchTaskTwoPhrases implements Callable<List<SearchResult>> {

    private File file;
    private String phrase1;
    private String phrase2;

    public SearchTaskTwoPhrases(File file, String phrase1, String phrase2) {
        checkNotNull(file);
        checkNotNull(phrase1);
        checkNotNull(phrase2);
        this.file = file;
        this.phrase1 = phrase1;
        this.phrase2 = phrase2;
    }

    @Override
    public List<SearchResult> call() throws Exception {
        SearchEngine engine = new SearchEngine();
        return engine.searchWithinFile(file, phrase1, phrase2);
    }

}
