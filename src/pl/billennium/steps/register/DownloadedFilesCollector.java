package pl.billennium.steps.register;

import pl.billennium.ExecutionParameter;
import pl.billennium.config.Configuration;
import pl.billennium.config.ConfigurationService;
import pl.billennium.steps.Step;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DownloadedFilesCollector implements Step {

    private static DownloadedFilesCollector instance;

    private Map<String, List<String>> dirsAndFiles;

    public static synchronized DownloadedFilesCollector getInstance() {

        if (instance == null) {
            instance = new DownloadedFilesCollector();
            instance.dirsAndFiles = new HashMap<>();
            instance.saveFileNames();
        }
        return instance;
    }

    public void saveFileNames() {
        Configuration configuration = ConfigurationService.getInstance().getConfiguration();
        try {
            List<Path> paths = Files.walk(Paths.get(configuration.getWorkingDir())).collect(Collectors.toList());

            for (Path p : paths) {
                if (Files.isDirectory(p)) {
                    String dir = p.getFileName().toString();
                    dirsAndFiles.put(dir, new ArrayList<>());
                } else {
                    String logFile = p.getFileName().toString();
                    String parent = p.getParent().getFileName().toString();
                    dirsAndFiles.get(parent).add(logFile);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<String, List<String>> getDirsAndFiles() {
        return dirsAndFiles;
    }

    public static void clearDirsAndFilesRegistry() {
        instance.dirsAndFiles = new HashMap<>();
    }

    @Override
    public String getName() {
        return "Rejestruje pliki";
    }

    @Override
    public boolean isSuccess() {
        return true;
    }

    @Override
    public void execute(ExecutionParameter parameter) {
        saveFileNames();
    }
}