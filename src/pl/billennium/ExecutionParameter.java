package pl.billennium;

import com.google.common.base.Strings;
import pl.billennium.config.Configuration;
import pl.billennium.config.ConfigurationService;
import pl.billennium.config.NetworkDisk;

import java.io.File;
import java.util.Date;
import java.util.UUID;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Created by ebos on 10/31/2017.
 */
public class ExecutionParameter {
    private String filePattern;
    private String phrase1;
    private String phrase2;
    private String searchSession;
    private Date dateFrom;
    private Date dateTo;
    private String servers;
    private Boolean onNetworkDrive;
    private NetworkDisk networkDisk;

    private ExecutionParameter(Builder builder) {
        this.filePattern = builder.filePattern;
        this.phrase1 = builder.phrase1;
        this.phrase2 = builder.phrase2;
        this.searchSession = builder.searchSession;
        this.dateFrom = builder.dateFrom;
        this.dateTo = builder.dateTo;
        this.servers = builder.servers;
        this.onNetworkDrive = builder.onNetworkDrive;
        this.networkDisk = builder.networkDisk;
    }

    public File getSessionDir() {
        Configuration configuration = ConfigurationService.getInstance().getConfiguration();
        return new File(configuration.getWorkingDir(), searchSession);
    }

    public String getFilePattern() {
        return filePattern;
    }

    public String getSearchSession() {
        return searchSession;
    }

    public String getPhrase1() {
        return phrase1;
    }

    public String getPhrase2() {
        return phrase2;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public String getServers() {
        return servers;
    }

    public boolean isSinglePhraseSearch() {
        return phrase1 != null && Strings.isNullOrEmpty(phrase2);
    }

    public Boolean getOnNetworkDrive() {
        return onNetworkDrive;
    }

    public NetworkDisk getNetworkDisk() {
        return networkDisk;
    }

    public static class Builder {
        private static final String REGEX = "(\\d-\\d){1}(,\\d-\\d)*";

        private String filePattern;
        private String phrase1;
        private String phrase2;
        private String searchSession;
        private Date dateFrom;
        private Date dateTo;
        private String servers;
        private Boolean onNetworkDrive;
        private NetworkDisk networkDisk;

        public Builder filePattern(String filePattern) {
            this.filePattern = filePattern;
            return this;
        }

        public Builder phrase1(String phrase1) {
            this.phrase1 = phrase1;
            return this;
        }

        public Builder phrase2(String phrase2) {
            this.phrase2 = phrase2;
            return this;
        }

        public Builder dateFrom(Date dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public Builder dateTo(Date dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public Builder servers(String servers) {
            this.servers = servers;
            return this;
        }

        public Builder onNetworkDrive(Boolean onNetworkDrive) {
            this.onNetworkDrive = onNetworkDrive;
            return this;
        }

        public Builder networkDisk(NetworkDisk networkDisk){
            this.networkDisk = networkDisk;
            return this;
        }

        public ExecutionParameter build() {
            this.searchSession = UUID.randomUUID().toString();
            checkState(!Strings.isNullOrEmpty(servers), "Nie podano serwerow");
            checkState(servers.matches(REGEX), "!servers.matches(REGEX)");
            checkNotNull(filePattern, "Nie podano wzorca");
            checkNotNull(dateFrom, "Nie podano daty od");
            checkState(!Strings.isNullOrEmpty(phrase1), "Nie podano frazy 1");
            checkState(phrase1.length() >= 3, "Fraza 1 zbyt krotka");
            checkState(onNetworkDrive != null, "Nie pobrano informacji czy szukać na dyskach sieciowych");

            return new ExecutionParameter(this);
        }
    }

}
