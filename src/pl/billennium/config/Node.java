package pl.billennium.config;

/**
 * Created by ebos on 11/2/2017.
 */
public class Node {

    private Integer number;
    private String name;

    public Integer getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Node{" +
                "number=" + number +
                ", name='" + name + '\'' +
                '}';
    }
}
