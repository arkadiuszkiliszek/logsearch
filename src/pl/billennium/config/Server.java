package pl.billennium.config;

import java.util.List;

/**
 * Created by ebos on 10/31/2017.
 */
public class Server {

    private String host;
    private List<Node> nodes;
    private Integer number;
    private Integer port;

    public String getHost() {
        return host;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public Integer getNumber() {
        return number;
    }

    public Integer getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "Server{" +
                "host='" + host + '\'' +
                ", nodes=" + nodes +
                ", number=" + number +
                '}';
    }

}
