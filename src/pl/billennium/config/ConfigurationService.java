package pl.billennium.config;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ebos on 10/31/2017.
 */
public final class ConfigurationService {

    private static ConfigurationService INSTANCE;

    private Configuration configuration;

    private ConfigurationService() {
    }

    public static synchronized ConfigurationService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ConfigurationService();
        }

        return INSTANCE;
    }

    public void init(String file) {
        Gson gson = new Gson();
        try {
            configuration = gson.fromJson(new FileReader(file),
                    Configuration.class);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public Configuration getConfiguration() {
        checkNotNull(configuration, "brak konfiguracji");
        return configuration;
    }

}
