package pl.billennium.config;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by ebos on 10/31/2017.
 */
public class Configuration {

    private String workingDir;

    private Boolean cleanWorkingDirOnStart;

    private String user;

    private String keyPath;

    private String passphrase;

    private List<Server> servers;

    private String logDateFormat;

    private String textEditor;

    private String archiveDir;

    private List<NetworkDisk> newtorkDisks;

    private String networkDirName;

    public String getWorkingDir() {
        return workingDir;
    }

    public Boolean getCleanWorkingDirOnStart() {
        return cleanWorkingDirOnStart;
    }

    public List<Server> getServers() {
        return servers;
    }

    public String getKeyPath() {
        return keyPath;
    }

    public String getUser() {
        return user;
    }

    public String getPassphrase() {
        return passphrase;
    }

    public String getLogDateFormat() {
        return logDateFormat;
    }

    public String getTextEditor() {
        return textEditor;
    }

    public String getArchiveDir() {
        return archiveDir;
    }

    public SimpleDateFormat prepareDateFormat() {
        return new SimpleDateFormat(logDateFormat);
    }

    public List<NetworkDisk> getNewtorkDisks() {
        return newtorkDisks;
    }

    public String getNetworkDirName() {
        return networkDirName;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "workingDir='" + workingDir + '\'' +
                ", cleanWorkingDirOnStart=" + cleanWorkingDirOnStart +
                ", user='" + user + '\'' +
                ", keyPath='" + keyPath + '\'' +
                ", passphrase='" + passphrase + '\'' +
                ", servers=" + servers +
                ", logDateFormat='" + logDateFormat + '\'' +
                ", textEditor='" + textEditor + '\'' +
                ", archiveDir='" + archiveDir + '\'' +
                ", newtorkDisks=" + newtorkDisks +
                ", networkDirName='" + networkDirName + '\'' +
                '}';
    }
}
