package pl.billennium.config;

import java.util.List;

public class NetworkDisk {

    private String host;
    private String localDiskName;
    private List<Node> nodes;
    private Integer number;

    public String getHost() {
        return host;
    }

    public String getLocalDiskName() {
        return localDiskName;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public Integer getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "NetworkDisk{" +
                "host='" + host + '\'' +
                ", localDiskName='" + localDiskName + '\'' +
                ", nodes=" + nodes +
                ", number=" + number +
                '}';
    }
}
