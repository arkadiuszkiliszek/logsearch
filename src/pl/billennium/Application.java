package pl.billennium;/**
 * Created by ebos on 11/9/2017.
 */

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import pl.billennium.config.Configuration;
import pl.billennium.config.ConfigurationService;

import java.io.File;
import java.io.IOException;

public class Application extends javafx.application.Application {

    private Parent rootNode;

    public static void main(String[] args) {
        ConfigurationService.getInstance().init(args[0]);

        clearWorkingDirIfNeeded();

        launch(args);
    }

    private static void clearWorkingDirIfNeeded() {
        Configuration configuration = ConfigurationService.getInstance().getConfiguration();
        if (configuration.getCleanWorkingDirOnStart()) {
            try {
                FileUtils.cleanDirectory(new File(configuration.getWorkingDir()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void init() throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ui/views/search.fxml"));
        rootNode = loader.load();
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setScene(new Scene(rootNode));
        primaryStage.setTitle("LogSearch");
        primaryStage.setOnCloseRequest(e->{
            Platform.exit();
            System.exit(0);
        });
        primaryStage.show();
    }
}
