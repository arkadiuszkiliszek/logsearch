package pl.billennium.ui.controllers;

import com.google.common.base.Strings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.apache.commons.io.FileUtils;
import pl.billennium.ExecutionParameter;
import pl.billennium.config.ConfigurationService;
import pl.billennium.config.NetworkDisk;
import pl.billennium.steps.StepsManager;
import pl.billennium.steps.search.SearchResult;
import pl.billennium.ui.views.SearchResultWrapper;
import pl.billennium.utils.DirUtil;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by ebos on 11/9/2017.
 */
public class SearchController {

    private SearchService searchService;

    @FXML
    private TableView<SearchResultWrapper> wyniki;

    @FXML
    private TextField fraza1;

    @FXML
    private TextField fraza2;

    @FXML
    private ComboBox<String> wzorzec;

    @FXML
    private VBox serwery;

    @FXML
    private DatePicker dataOd;

    @FXML
    private DatePicker dataDo;

    @FXML
    private Spinner<Integer> czasOd;

    @FXML
    private Spinner<Integer> czasDo;

    @FXML
    private Button otworzPlik;

    @FXML
    private Button otworzLog;

    @FXML
    private Button kopiujLinie;

    @FXML
    private Button kopiujLog;

    @FXML
    private Button szukaj;

    @FXML
    private Button restart;

    @FXML
    private Button czyscFolder;

    @FXML
    private ProgressBar progress;

    @FXML
    private Label trafien;

    @FXML
    private ComboBox dyskSieciowyWybor;

    @FXML
    private CheckBox szukajSieciowe;

    @FXML
    protected void initialize() {
        searchService = new SearchService();
        List<NetworkDisk> dyskiSieciowe = ConfigurationService.getInstance().getConfiguration().getNewtorkDisks();
        dataOd.setValue(LocalDate.now());
        dataDo.setValue(LocalDate.now());
        czasOd.getValueFactory().setConverter(new SpinnerStringConverter());
        czasDo.getValueFactory().setConverter(new SpinnerStringConverter());
        czasOd.getValueFactory().setValue(12);
        czasDo.getValueFactory().setValue(12);
        wzorzec.getSelectionModel().selectFirst();
        wyniki.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            otworzPlik.setDisable(newValue == null);
            otworzLog.setDisable(newValue == null);
            kopiujLinie.setDisable(newValue == null);
            kopiujLog.setDisable(newValue == null);
        });
        otworzPlik.setDisable(true);
        otworzLog.setDisable(true);
        kopiujLinie.setDisable(true);
        kopiujLog.setDisable(true);
        czyscFolder.setDisable(true);
        progress.managedProperty().bind(progress.visibleProperty());
        trafien.managedProperty().bind(trafien.visibleProperty());

        ArrayList<String> dyskiSiecioweNazwy = new ArrayList<>();
        for(NetworkDisk networkDisk: dyskiSieciowe){
            dyskiSiecioweNazwy.add(networkDisk.getHost());
        }
        ObservableList<String> dyskiWartosciWyswietlane =
                FXCollections.observableArrayList(dyskiSiecioweNazwy);
        dyskSieciowyWybor.getItems().addAll(dyskiWartosciWyswietlane);

        dyskSieciowyWybor.getSelectionModel().selectFirst();
        dyskSieciowyWybor.setDisable(true);

        Callback<TableColumn<SearchResultWrapper, String>, TableCell<SearchResultWrapper, String>> callback = param -> {
            TableCell<SearchResultWrapper, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            text.wrappingWidthProperty().bind(param.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            return cell;
        };

        TableColumn<SearchResultWrapper, String> columnLine = (TableColumn<SearchResultWrapper, String>) wyniki.getColumns().get(2);
        TableColumn<SearchResultWrapper, String> columnLog = (TableColumn<SearchResultWrapper, String>) wyniki.getColumns().get(3);
        columnLine.setCellFactory(callback);
        columnLog.setCellFactory(callback);
    }

    @FXML
    protected void all3OnAcction(ActionEvent event) {
        Set<Node> checkBoxes = serwery.lookupAll("CheckBox");
        checkBoxes.stream().map(CheckBox.class::cast)
                .filter(a -> a.getId().endsWith("_3")).forEach(a -> a.setSelected(true));
    }

    @FXML
    protected void all2OnAcction(ActionEvent event) {
        Set<Node> checkBoxes = serwery.lookupAll("CheckBox");
        checkBoxes.stream().map(CheckBox.class::cast)
                .filter(a -> a.getId().endsWith("_2")).forEach(a -> a.setSelected(true));
    }

    @FXML
    protected void all1OnAcction(ActionEvent event) {
        Set<Node> checkBoxes = serwery.lookupAll("CheckBox");
        checkBoxes.stream().map(CheckBox.class::cast)
                .filter(a -> a.getId().endsWith("_1")).forEach(a -> a.setSelected(true));
    }

    @FXML
    protected void allOnAcction(ActionEvent event) {
        Set<Node> checkBoxes = serwery.lookupAll("CheckBox");
        checkBoxes.stream().map(CheckBox.class::cast).forEach(a -> a.setSelected(true));
    }

    @FXML
    protected void clearOnAcction(ActionEvent event) {
        Set<Node> checkBoxes = serwery.lookupAll("CheckBox");
        checkBoxes.stream().map(CheckBox.class::cast).forEach(a -> a.setSelected(false));
    }

    @FXML
    protected void openFileOnAcction(ActionEvent event) {
        SearchResultWrapper selectedItem = wyniki.getSelectionModel().getSelectedItem();
        String textEditor = ConfigurationService.getInstance().getConfiguration().getTextEditor();
        try {
            Process process = new ProcessBuilder(textEditor, selectedItem.getSource().getFile().getAbsolutePath()).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @FXML
    void unlockChoseNetworkDiskCombo(MouseEvent event) {
        CheckBox checkbox = (CheckBox) event.getSource();
        if(checkbox.isSelected())
            dyskSieciowyWybor.setDisable(false);
        else
            dyskSieciowyWybor.setDisable(true);
    }

    @FXML
    protected void openLogOnAcction(ActionEvent event) {
        SearchResultWrapper selectedItem = wyniki.getSelectionModel().getSelectedItem();
        try {
            File tmpFile = File.createTempFile("log_search", ".log");
            tmpFile.deleteOnExit();
            FileUtils.writeStringToFile(tmpFile, selectedItem.getSource().getLogMessage(), Charset.forName("UTF-8"));
            String textEditor = ConfigurationService.getInstance().getConfiguration().getTextEditor();
            Process process = new ProcessBuilder(textEditor, tmpFile.getAbsolutePath()).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    protected void kopiujLinieOnAction(ActionEvent event) {
        SearchResultWrapper selectedItem = wyniki.getSelectionModel().getSelectedItem();
        Clipboard clipboard = Clipboard.getSystemClipboard();
        final ClipboardContent content = new ClipboardContent();
        content.putString(selectedItem.getSource().getLine());
        clipboard.setContent(content);
    }

    @FXML
    protected void kopiujLogOnAction(ActionEvent event) {
        SearchResultWrapper selectedItem = wyniki.getSelectionModel().getSelectedItem();
        Clipboard clipboard = Clipboard.getSystemClipboard();
        final ClipboardContent content = new ClipboardContent();
        content.putString(selectedItem.getSource().getLogMessage());
        clipboard.setContent(content);
    }

    @FXML
    protected void wyszukajOnAction(ActionEvent event) {
        try {
            searchService = new SearchService();
            System.gc();
            searchService.start();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Blad");
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    protected void restartOnAction(ActionEvent event) {
        try {
            System.out.println("Anulowano");
            searchService.cancel();
            this.searchService = new SearchService();
            System.gc();
        } catch (RuntimeException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Blad");
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    protected void clearWorkingFolderOnAction(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Czyszczenie folderu");
        alert.setHeaderText("Czy na pewno chcesz usunąć pobrane logi?");
        alert.setOnCloseRequest(event -> alert.close());

        Optional<ButtonType> buttonType = alert.showAndWait();
        if (buttonType.get() == ButtonType.OK) {
            DirUtil.clearWorkingDir();
        } else if (buttonType.get() == ButtonType.CANCEL) {
            alert.close();

            /*DirUtil.clearWorkingDir();*/
        }
    }

    private Date prepareDate(DatePicker date, Spinner<Integer> hour) {
        if (date.getValue() != null) {
            Date dateTo = Date.from(date.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
            Calendar instance = Calendar.getInstance();
            instance.setTime(dateTo);
            instance.set(Calendar.HOUR_OF_DAY, hour.getValue());
            return instance.getTime();
        }
        return null;
    }

    private Boolean getSearchOnNetworkDiskInfo(){
        return szukajSieciowe.isSelected();
    }
    private String getChoseNetworkDisk(){
        return String.valueOf(dyskSieciowyWybor.getValue());
    }

    private String collectServers() {
        Set<Node> checkBoxes = serwery.lookupAll("CheckBox");
        String servers = checkBoxes.stream().map(CheckBox.class::cast)
                .filter(a -> a.isSelected())
                .filter(a -> !a.getId().equals("szukajSieciowe"))
                .map(a -> a.getText()).collect(Collectors.joining(","));
        return servers;
    }


    static class SpinnerStringConverter extends StringConverter<Integer> {
        @Override
        public String toString(Integer object) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH");
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, object);
            return sdf.format(calendar.getTime());
        }

        @Override
        public Integer fromString(String string) {
            return Integer.valueOf(string);
        }
    }

    class SearchService extends Service<Void> {

        @Override
        protected Task<Void> createTask() {
            String servers = collectServers();

            Date dateFrom = prepareDate(dataOd, czasOd);
            Date dateTo = prepareDate(dataDo, czasDo);
            Boolean searchOnNewtorkDisk = getSearchOnNetworkDiskInfo();

            ExecutionParameter arg = new ExecutionParameter.Builder()
                    .phrase1(fraza1.getText())
                    .phrase2(Strings.isNullOrEmpty(fraza2.getText()) ? null : fraza2.getText())
                    .servers(servers)
                    .filePattern(wzorzec.getValue())
                    .onNetworkDrive(searchOnNewtorkDisk)
                    .networkDisk(searchOnNewtorkDisk ? getNetworkDisk() : null)
                    .dateFrom(dateFrom).dateTo(dateTo).build();

            return new SearchTask(arg);
        }

        @Override
        protected void cancelled() {
            szukaj.setDisable(false);
            czyscFolder.setDisable(false);
            progress.setProgress(0);
            progress.setVisible(false);
            trafien.setVisible(true);
            trafien.setText(String.format("Anulowano"));

        }

        @Override
        protected void succeeded() {
            szukaj.setDisable(false);
            czyscFolder.setDisable(false);
            progress.setProgress(0);
            progress.setVisible(false);
            trafien.setVisible(true);
            trafien.setText(String.format("Ilość trafień: %s", wyniki.getItems().size()));
            if (wyniki.getItems().isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("LogSearch");
                alert.setHeaderText("Brak trafien");
                alert.showAndWait();
            }
        }

        @Override
        protected void running() {
            szukaj.setDisable(true);
            czyscFolder.setDisable(true);
            wyniki.getItems().clear();
            progress.setVisible(true);
            trafien.setVisible(false);
            progress.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
        }
    }

    private NetworkDisk getNetworkDisk() {
        String chooseDisk = getChoseNetworkDisk();
        List<NetworkDisk> networkDisks = ConfigurationService.getInstance().getConfiguration().getNewtorkDisks();
        for(NetworkDisk networkDisk: networkDisks){
            if(networkDisk.getHost().equals(chooseDisk))
                return networkDisk;
        }
        return null;
    }

    class SearchTask extends Task<Void> {

        private ExecutionParameter arg;

        public SearchTask(ExecutionParameter arg) {
            this.arg = arg;
        }

        @Override
        protected Void call() throws Exception {
            StepsManager manager = new StepsManager();

            List<SearchResult> results = manager.execute(arg);
            List<SearchResultWrapper> wrappers = results.stream().map(a -> new SearchResultWrapper(a, arg.getPhrase1())).collect(Collectors.toList());

            wyniki.getItems().setAll(wrappers);
            wyniki.refresh();
            return null;
        }
    }
}