package pl.billennium.ui.views;

import pl.billennium.steps.search.SearchResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

/**
 * Created by ebos on 11/9/2017.
 */
public class SearchResultWrapper {

    private SearchResult source;
    private String phrase1;
    private String message;
    private String line;

    public SearchResultWrapper(SearchResult source, String phrase1) {
        this.source = source;
        this.phrase1 = phrase1;
        init();
    }

    private void init() {
        this.message = readFirstMessageLine(source.getLogMessage());
        if (message.length() > 200) {
            message = message.substring(0, 200) + "...";
        }
        prepareLine();
    }

    private void prepareLine() {
        int length = 200 + phrase1.length();
        int padding = length / 2;

        String line = source.getLine().trim();
        int index = line.toLowerCase().indexOf(phrase1.toLowerCase());
        boolean leftDots = false;
        boolean rightDots = false;
        int startIndex;
        int endIndex;
        if (index < padding) {
            startIndex = 0;
        } else {
            startIndex = index - padding;
            leftDots = true;
        }

        if (startIndex + length > line.length()) {
            endIndex = line.length();
        } else {
            endIndex = startIndex + length;
            rightDots = true;
        }

        this.line = (leftDots ? "..." : "") + line.substring(startIndex, endIndex) + (rightDots ? "..." : "");
    }

    private String readFirstMessageLine(String src) {
        BufferedReader reader = new BufferedReader(new StringReader(src));
        try {
            return reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getFile() {
        return source.getFile().getName();
    }

    public String getLineNumber() {
        return source.getLineNumber().toString();
    }

    public String getLine() {
        return line;
    }

    public String getMessage() {
        return message;
    }

    public SearchResult getSource() {
        return source;
    }
}
